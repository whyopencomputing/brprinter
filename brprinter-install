#!/bin/bash
# Script d'installation pour imprimantes Brother embarqué sur les ordinateurs Why!
# Auteur: Cedric Puchalver <cedric.puchalver@gmail.com>
# Version: 1.0

#########################################################################
# This program is free software: you can redistribute it and/or modify	#
# it under the terms of the GNU General Public License as published by	#
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program.  If not, see <http://www.gnu.org/licenses/>. #
#########################################################################

. /lib/lsb/init-functions

TEXTDOMAIN="brprinter-install"
TEXTDOMAINDIR="$(dirname "$0")/locale"

# cette fonction sert à definir les variables utilisées par le script
function do_set_vars() {
	TEMP_DIR=$(mktemp -d --tmpdir=/tmp brprinter-install_XXXXXXXX)
	CODENAME=$(lsb_release -cs)
	PWD=$(dirname $0)
	ARCH=$(uname -m)
	BLUE="\\033[1;34m"
	RED="\\033[1;31m"
	RESETCOLOR="\\033[0;0m"
	SCANKEY_TOOL=2
	UDEV_RULES="/lib/udev/rules.d/40-libsane.rules"
}

# cette fonction vérifie si whiptail est installé (devrait etre le cas par defaut)
function do_check_whiptail() {
	dpkg -l | awk '{print $2}' | grep -q ^whiptail$
	if [[ $? != "0" ]]; then
		echo -e ${RED}$"You must install package 'whiptail' in order to use this script"${RESETCOLOR}
		exit 1
	fi
}

# cette fonction affiche la premiere fenetre d'installation
function do_show_help() {
	whiptail --msgbox --backtitle=$"Installation wizard for Brother printers" $"This wizard will guide you through the installation of your Brother printer.\n\nUse the arrow keys or TAB to move the cursor. Press ENTER to validate your choice." 11 70
}

# cette fonction vérifie si le script est lancé avec les droits du super-utilisateur
function do_check_uid() {
	if [[ "${UID}" != "0" ]]; then
		echo -e ${RED}$"You must run this script as root"${RESETCOLOR}
		do_clean_install_files
		exit 1
	fi
}

# cette fonction affiche la liste des familles d'imprimantes
# on quitte le programme si aucune famille n'est choisie (touche annuler)
function do_get_family() {
	FAMILY=$(whiptail --menu --ok-button $"Next" --cancel-button $"Quit" --backtitle=$"Installation wizard for Brother printers" $"\nChoose your printer family :" 13 70 4 "DCP" "" "HL" "" "FAX" "" "MFC" "" 3>&1 1>&2 2>&3)
	if [[ -z "${FAMILY}" ]]; then
		do_clean_install_files
		exit 1
	fi
}

# cette fonction affiche la liste des modèles d'imprimantes en fonction de la famille choisie
# on quitte le programme si aucun modèle n'est choisi (touche annuler)
function do_get_printer() {
	MODEL=$(whiptail --menu --ok-button $"Next" --cancel-button $"Quit" --backtitle=$"Installation wizard for Brother printers" $"\nChoose your printer model :" 15 70 6 $(grep "${FAMILY}-" ${PWD}/drivers_printer|sed 's/$/ \r/g;s/#//g') 3>&1 1>&2 2>&3)
	# On quitte si la variable MODEL est vide
	if [[ -z "${MODEL}" ]]; then
		do_clean_install_files
		exit 1
	fi
}

# cette fonction affiche le type de connexion utilisée
# on quitte le programme si aucune connexion n'est choisie (touche annuler)
function do_get_connection() {
	CONNECTION=$(whiptail --menu --ok-button $"Next" --cancel-button $"Quit" --backtitle=$"Installation wizard for Brother printers" $"\nChoose your printer connection :" 11 70 2 $"USB" "" $"Network" "" 3>&1 1>&2 2>&3)
	if [[ "${CONNECTION}" == $"Network" ]]; then
		do_get_ip_addr
	fi
	if [[ -z "${CONNECTION}" ]]; then
		do_clean_install_files
		exit 1
	fi
}

# cette fonction demande l'adresse ip de l'imprimante
function do_get_ip_addr() {
	IP=$(whiptail --inputbox --ok-button $"Next" --cancel-button $"Quit" --backtitle=$"Installation wizard for Brother printers" $"\nEnter your printer IP address :" 9 70 3>&1 1>&2 2>&3)
	if [[ -z "${IP}" ]]; then
		do_clean_install_files
		exit 1
	fi
}

# cette fonction demande confirmation des choix de l'utilisateur
# on quitte le programme si on appuie sur la touche annuler
function do_confirm() {
	case ${CONNECTION} in
		$"USB")
			CONFIRM=$(whiptail --yesno --backtitle=$"Installation wizard for Brother printers" $"Installation summary :\n\n- Model : ${MODEL}\n- Connection : ${CONNECTION}\n\nDo you want to continue?" 13 70 3>&1 1>&2 2>&3)
		;;
		$"Network")
			CONFIRM=$(whiptail --yesno --backtitle=$"Installation wizard for Brother printers" $"Installation summary :\n\n- Model : ${MODEL}\n- Connection : ${CONNECTION}\n- IP address : ${IP}\n\nDo you want to continue?" 13 70 3>&1 1>&2 2>&3)
		;;
	esac
	if [[ $? == 1 ]]; then
		do_clean_install_files
		exit 1
	fi
}

# cette fonction sert à vérifier que tous les pré-requis sont remplis
# voir page : http://support.brother.com/g/s/id/linux/en/index.htm?c=us_ot&lang=en&comple=on&redirect=on
function do_prerequisites() {
	echo -e ${BLUE}$"Checking prerequisites"${RESETCOLOR}
	log_action_begin_msg $"Updating packages list"
	apt-get update -qq
	log_action_end_msg $?
	# on vérifie que le paquet multiarch-support est installé et on l'installe le cas échéant (Ubuntu 64-bits seulement)
	if [[ ${ARCH} == "x86_64" ]]; then
		log_action_begin_msg $"Looking for package 'multiarch-support'"
		if dpkg -l | awk '{print $2}' | egrep -q ^multiarch-support$ ; then
			log_action_end_msg $?
		else
			log_action_end_msg 1
			log_action_begin_msg $"Installing package 'multiarch-support'"
			apt-get install -qq -y multiarch-support
			log_action_end_msg $?
		fi
	fi
	# on vérifie que le paquet cups est installé et on l'installe le cas échéant
	log_action_begin_msg $"Looking for package 'cups'"
	if dpkg -l | awk '{print $2}' | egrep -q ^cups$ ; then
		log_action_end_msg $?
	else
		log_action_end_msg 1
		log_action_begin_msg $"Installing package 'cups'"
		apt-get install -qq -y cups
		log_action_end_msg $?
	fi
	# on vérifie que le paquet sane-utils est installé et on l'installe le cas échéant
	log_action_begin_msg $"Looking for package 'sane-utils'"
	if dpkg -l | awk '{print $2}' | egrep -q ^sane-utils$; then
		log_action_end_msg $?
	else
		log_action_end_msg 1
		log_action_begin_msg $"Installing package 'sane-utils'"
		apt-get install -qq -y sane-utils
		log_action_end_msg $?
	fi
	# on vérifie que le paquet csh est installé et on l'installe le cas échéant (uniquement pour certaines imprimantes)
	for i in DCP-110C DCP-115C DCP-117C DCP-120C DCP-310CN DCP-315CN DCP-340CW FAX-1815C FAX-1820C FAX-1835C FAX-1840C FAX-1920CN FAX-1940CN FAX-2440C MFC-210C MFC-215C MFC-3220C MFC-3240C MFC-3320CN MFC-3340CN MFC-3420C MFC-3820CN MFC-410CN MFC-420CN MFC-425CN MFC-5440CN MFC-5840CN MFC-620CN MFC-640CW MFC-820CW; do
		if [[ ${MODEL} == "$i" ]]; then
			log_action_begin_msg $"Looking for package 'csh'"
			if [[ ! -x /bin/csh ]]; then
				log_action_end_msg 1
				log_action_begin_msg $"Installing package 'csh'"
				apt-get install -qq -y csh
				log_action_end_msg $?
			else
				log_action_end_msg $?
			fi
		fi
	done
	# on vérifie que le dossier /usr/share/cups/model existe et on le crée le cas échéant
	log_action_begin_msg $"Looking for folder '/usr/share/cups/model'"
	if [[ -d /usr/share/cups/model ]]; then
		log_action_end_msg $?
	else
		log_action_end_msg 1
		log_action_begin_msg $"Creating folder '/usr/share/cups/model'"
		mkdir -p /usr/share/cups/model
		log_action_end_msg $?
	fi
	# on vérifie que le dossier /var/spool/lpd existe et on le crée le cas échéant
	log_action_begin_msg $"Looking for folder '/var/spool/lpd'"
	if [[ -d /var/spool/lpd ]]; then
		log_action_end_msg $?
	else
		log_action_end_msg 1
		log_action_begin_msg $"Creating folder '/var/spool/lpd'"
		mkdir -p /var/spool/lpd
		log_action_end_msg $?
	fi
	# on vérifie que le lien symbolique /etc/init.d/lpd existe et on le crée le cas échéant (uniquement pour certaines imprimantes)
	for i in DCP-1000 DCP-1400 DCP-8020 DCP-8025D DCP-8040 DCP-8045D DCP-8060 DCP-8065DN FAX-2850 FAX-2900 FAX-3800 FAX-4100 FAX-4750e FAX-5750e HL-1030 HL-1230 HL-1240 HL-1250 HL-1270N HL-1430 HL-1440 HL-1450 HL-1470N HL-1650 HL-1670N HL-1850 HL-1870N HL-5030 HL-5040 HL-5050 HL-5070N HL-5130 HL-5140 HL-5150D HL-5170DN HL-5240 HL-5250DN HL-5270DN HL-5280DW HL-6050 HL-6050D MFC-4800 MFC-6800 MFC-8420 MFC-8440 MFC-8460N MFC-8500 MFC-8660DN MFC-8820D MFC-8840D MFC-8860DN MFC-8870DW MFC-9030 MFC-9070 MFC-9160 MFC-9180 MFC-9420CN MFC-9660 MFC-9700 MFC-9760 MFC-9800 MFC-9860 MFC-9880; do
		if [[ ${MODEL} == "$i" ]]; then
			log_action_begin_msg $"Looking for symlink '/etc/init.d/lpd ~> /etc/init.d/cups'"
			if [[ -L /etc/init.d/lpd ]]; then
				log_action_end_msg 0
			else
				log_action_end_msg 1
				log_action_begin_msg $"Creating symlink '/etc/init.d/lpd ~> /etc/init.d/cups'"
				ln -s /etc/init.d/cups /etc/init.d/lpd
				log_action_end_msg $?
			fi
		fi
	done
}

# cette fonction sert à installer les pilotes de l'imprimante
function do_install_printer_driver() {
	echo -e ${BLUE}$"Installing printer drivers"${RESETCOLOR}
	case ${MODEL} in
		# cas d'installation MFC-8220
		MFC-8220)
			log_action_begin_msg $"Downloading file : BR8220_2_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR8220_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR8220_2_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR8220_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BR8220_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR8220_2_GPL.ppd
			chown root:root /usr/share/cups/model/BR8220_2_GPL.ppd
			ln -sf /usr/share/cups/model/BR8220_2_GPL.ppd /usr/share/ppd/BR8220_2_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation MFC-8640D
		MFC-8640D)
			log_action_begin_msg $"Downloading file : BR8640_2_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR8640_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR8640_2_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR8640_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BR8640_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR8640_2_GPL.ppd
			chown root:root /usr/share/cups/model/BR8640_2_GPL.ppd
			ln -sf /usr/share/cups/model/BR8640_2_GPL.ppd /usr/share/ppd/BR8640_2_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-2460 et HL-2460N
		HL-2460|HL-2460N)
			log_action_begin_msg $"Downloading file : BRHL24_2_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BRHL24_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BRHL24_2_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BRHL24_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BRHL24_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BRHL24_2_GPL.ppd
			chown root:root /usr/share/cups/model/BRHL24_2_GPL.ppd
			ln -sf /usr/share/cups/model/BRHL24_2_GPL.ppd /usr/share/ppd/BRHL24_2_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-2600CN
		HL-2600CN)
			log_action_begin_msg $"Downloading file : BR2600CN_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR2600CN_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR2600CN_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR2600CN_GPL.ppd.gz
			cp ${TEMP_DIR}/BR2600CN_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR2600CN_GPL.ppd
			chown root:root /usr/share/cups/model/BR2600CN_GPL.ppd
			ln -sf /usr/share/cups/model/BR2600CN_GPL.ppd /usr/share/ppd/BR2600CN_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-2700CN
		HL-2700CN)
			log_action_begin_msg $"Downloading file : BR2700CN_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR2700_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR2700CN_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR2700_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BR2700_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR2700_2_GPL.ppd
			chown root:root /usr/share/cups/model/BR2700_2_GPL.ppd
			ln -sf /usr/share/cups/model/BR2700_2_GPL.ppd /usr/share/ppd/BR2700_2_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-3260N
		HL-3260N)
			log_action_begin_msg $"Downloading file : BRHL32_3_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BRHL32_3_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BRHL32_3_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BRHL32_3_GPL.ppd.gz
			cp ${TEMP_DIR}/BRHL32_3_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BRHL32_3_GPL.ppd
			chown root:root /usr/share/cups/model/BRHL32_3_GPL.ppd
			ln -sf /usr/share/cups/model/BRHL32_3_GPL.ppd /usr/share/ppd/BRHL32_3_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-3450CN
		HL-3450CN)
			log_action_begin_msg $"Downloading file : BR3450CN_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR3450CN_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR3450CN_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR3450CN_GPL.ppd.gz
			cp ${TEMP_DIR}/BR3450CN_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR3450CN_GPL.ppd
			chown root:root /usr/share/cups/model/BR3450CN_GPL.ppd
			ln -sf /usr/share/cups/model/BR3450CN_GPL.ppd /usr/share/ppd/BR3450CN_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-7050 ET HL-7050N
		HL-7050|HL-7050N)
			log_action_begin_msg $"Downloading file : BR7050_2_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR7050_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR7050_2_GPL.ppd"
			gzip -fd ${TEMP_DIR}/BR7050_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BR7050_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR7050_2_GPL.ppd
			chown root:root /usr/share/cups/model/BR7050_2_GPL.ppd
			ln -sf /usr/share/cups/model/BR7050_2_GPL.ppd /usr/share/ppd/BR7050_2_GPL.ppd
			log_action_end_msg $?
		;;
		# CAS D'INTALLATION HL-S7000DN
		HL-S7000DN)
			log_action_begin_msg $"Downloading file : BRP7000E_GPL.PPD.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BRP7000E_GPL.PPD.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BRP7000E_GPL.PPD"
			gzip -fd ${TEMP_DIR}/BRP7000E_GPL.PPD.gz
			cp ${TEMP_DIR}/BRP7000E_GPL.PPD /usr/share/cups/model/BRP7000E_GPL.ppd
			chmod 644 /usr/share/cups/model/BRP7000E_GPL.ppd
			chown root:root /usr/share/cups/model/BRP7000E_GPL.ppd
			ln -sf /usr/share/cups/model/BRP700E_GPL.ppd /usr/share/ppd/BRP7000E_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation HL-8050N
		HL-8050N)
			log_action_begin_msg $"Downloading file : BR8050_2_GPL.ppd.gz"
			wget -qcP ${TEMP_DIR} http://www.brother.com/pub/bsc/linux/dlf/BR8050_2_GPL.ppd.gz
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : BR8050_2_GPL.ppd.gz"
			gzip -fd ${TEMP_DIR}/BR8050_2_GPL.ppd.gz
			cp ${TEMP_DIR}/BR8050_2_GPL.ppd /usr/share/cups/model
			chmod 644 /usr/share/cups/model/BR8050_2_GPL.ppd
			chown root:root /usr/share/cups/model/BR8050_2_GPL.ppd
			ln -sf /usr/share/cups/model/BR8050_2_GPL.ppd /usr/share/ppd/BR8050_2_GPL.ppd
			log_action_end_msg $?
		;;
		# cas d'installation DES AUTRES MODÈLES
		*)
			n=0
			for URL in $(egrep -A 2 ${MODEL}$ ${PWD}/drivers_printer|grep -v ${MODEL}); do
				PACKAGE[${n}]=$(basename ${URL})
				log_action_begin_msg $"Downloading file : $(basename ${URL})"
				wget -qcP ${TEMP_DIR} ${URL}
				log_action_end_msg $?
				log_action_begin_msg $"Installing file : $(basename ${URL})"
				dpkg -i --force-all "${TEMP_DIR}/$(basename ${URL})" > /dev/null
				log_action_end_msg $?
				n=${n}+1
			done
		;;
	esac
	# on lance la fonction de configuration de l'imprimante
	do_config_printer
}

# cette fonction sert à installer les pilotes du scanner
function do_install_scanner_driver() {
	if grep -q "${MODEL}" ${PWD}/drivers_scanner; then
		echo -e ${BLUE}$"Installing scanner drivers"${RESETCOLOR}
		echo "Variables : scankey_tool= $SCANKEY_TOOL model= $MODEL pwd= $PWD "
				
		case ${ARCH} in
			# cas d'installation 32 bits
			i386|i486|i686)
				for URL in $(egrep -A ${SCANKEY_TOOL} ${MODEL}$ ${PWD}/drivers_scanner|grep "i386"); do
					log_action_begin_msg $"Downloading file : $(basename ${URL})"
					wget -qcP ${TEMP_DIR} ${URL}
					log_action_end_msg $?
					log_action_begin_msg $"Installing file : $(basename ${URL})"
					dpkg -i --force-all "${TEMP_DIR}/$(basename ${URL})" > /dev/null
					log_action_end_msg $?
				done
			;;
			# cas d'installation 64 bits
			x86_64|amd64)
			    for URL in $(egrep -A ${SCANKEY_TOOL} ${MODEL}$ ${PWD}/drivers_scanner|grep "amd64"); do
					log_action_begin_msg $"Downloading file : $(basename ${URL})"
					wget -qcP ${TEMP_DIR} ${URL}
					log_action_end_msg $?
					log_action_begin_msg $"Installing file : $(basename ${URL})"
					dpkg -i --force-all "${TEMP_DIR}/$(basename ${URL})" > /dev/null
					log_action_end_msg $?
				done
			;;
		esac
		# on télécharge et installe le paquet brother-udev-rule-type1-1.0.0-1.all.deb
		#echo "Here 10.2"
		URL="http://www.brother.com/pub/bsc/linux/dlf/brother-udev-rule-type1-1.0.0-1.all.deb"
		log_action_begin_msg $"Downloading file : $(basename ${URL})"
		wget -qcP ${TEMP_DIR} ${URL}
		log_action_end_msg $?
		log_action_begin_msg $"Installing file : $(basename ${URL})"
		dpkg -i "${TEMP_DIR}/$(basename ${URL})" > /dev/null
		log_action_end_msg $?
		# on lance la fonction de configuration du scanner
		#echo "Here 10.3"
		do_config_scanner
	fi
}

# cette fonction sert à installer les pilotes du fax
function do_install_fax_driver() {
	# on cherche les pilotes du fax parmis le fichier drivers_fax
	if grep -q "${MODEL}" ${PWD}/drivers_fax; then
		echo -e ${BLUE}$"Installing fax drivers"${RESETCOLOR}
		# on telecharge et installe chaque paquet
		for URL in $(egrep -A 2 ${MODEL}$ ${PWD}/drivers_fax|grep -v ${MODEL}); do
			log_action_begin_msg $"Downloading file : $(basename ${URL})"
			wget -qcP ${TEMP_DIR} ${URL}
			log_action_end_msg $?
			log_action_begin_msg $"Installing file : $(basename ${URL})"
			dpkg -i --force-all "${TEMP_DIR}/$(basename ${URL})" > /dev/null
			log_action_end_msg $?
		done
		# on lance la fonction de configuration du fax
		do_config_fax
	fi
}

# fonction de configuration de l'imprimante
function do_config_printer() {
	# on définit le nom de l'imprimante à ajouter dans cups
	PRINTER_NAME=$(echo ${MODEL}|sed 's/'-'//g'|tr [a-z] [A-Z])
	# on cherche le fichier ppd à utiliser pour l'ajout de l'imprimante
	case ${MODEL} in
		# MFC-8220
		MFC-8220)
			PPD="/usr/share/cups/model/BR8220_2_GPL.ppd"
		;;
		# HL-2406 et HL-2460N
		HL-2460|HL-2460N)
			PPD="/usr/share/cups/model/BRHL24_2_GPL.ppd"
		;;
		# HL-2600CN
		HL-2600CN)
			PPD="/usr/share/cups/model/BR2600CN_GPL.ppd"
		;;
		# HL-2700CN
		HL-2700CN)
			PPD="/usr/share/cups/model/BR2700_2_GPL.ppd"
		;;
		# HL-3260N
		HL-3260N)
			PPD="/usr/share/cups/model/BRHL32_3_GPL.ppd"
		;;
		# HL-3450CN
		HL-3450CN)
			PPD="/usr/share/cups/model/BR3450CN_GPL.ppd"
		;;
		# HL-S7000DN
		HL-S7000DN)
			PPD="/usr/share/cups/model/BRP7000E_GPL.ppd"
		;;
		# HL-7050 et HL-7050N
		HL-7050|HL-7050N)
			PPD="/usr/share/cups/model/BR7050_2_GPL.ppd"
		;;
		# HL-8050N
		HL-8050N)
			PPD="/usr/share/cups/model/BR8050_2_GPL.ppd"
		;;
		# DCP-115C, DCP-117C, DCP-120C, DCP-315CN, DCP-340CW, MFC-210C, MFC-215C, MFC-425CN, MFC-640CW et MFC-820CW
		DCP-115C|DCP-117C|DCP-120C|DCP-315CN|DCP-340CW|MFC-210C|MFC-215C|MFC-425CN|MFC-640CW|MFC-820CW)
			PPD="/usr/share/cups/model/brmfc210c_cups.ppd"
		;;
		# les fichiers ppd des autres modeles devraient etre installés avec les pilotes
		*)
			PPD=$(for num in 0 1; do dpkg --contents ${TEMP_DIR}/${PACKAGE[${num}]}|awk '{print $6}'|grep ppd|sed 's/^.\//\//'; done)
		;;
	esac
	# on ajoute l'imprimante dans CUPS
	case ${CONNECTION} in
		# imprimante USB
		USB)
			log_action_begin_msg $"Adding USB printer to CUPS"
			lpadmin -p ${PRINTER_NAME} -E -v usb://Brother/${MODEL} -P ${PPD}
			log_action_end_msg $?
		;;
		# imprimante réseau
		Network)
			log_action_begin_msg $"Adding network printer to CUPS"
			lpadmin -p ${PRINTER_NAME} -E -v lpd://${IP}/binary_p1 -P ${PPD}
			log_action_end_msg $?
		;;
	esac
	# parfois un meme pilote est utilisé pour plusieurs imprimantes (voir par exemple le cas de colibri : http://www.swisslinux.org/forum/viewtopic.php?pid=21170#p21170)
	# les pilotes installent automatiquement l'imprimante de la serie
	# les fix suivants suppriment l'imprimante installée automatiquement
	# TODO: trouver une meilleure solution, par exemple renommer l'imprimante installée plutot que de la supprimer
	case ${MODEL} in
		# fix pour régler le problème des imprimantes utilisant le même pilote que la MFC-210C
		# en supprimant l'imprimante nommée MFC210C
		DCP-115C|DCP-117C|DCP-120C|DCP-315CN|DCP-340CW|MFC-215C|MFC-425CN|MFC-640CW|MFC-820CW)
			lpadmin -E -x MFC210C
		;;
		# fix pour régler le problème des imprimantes utilisant le même pilote que la MFC-1818
		# en supprimant l'imprimante nommée MFC1818
		MFC-1810|MFC-1810R|MFC-1811|MFC-1815|MFC-1815R)
			lpadmin -E -x MFC-1818
		;;
		# fix pour régler le problème des imprimantes utilisant le même pilote que la DCP-1610W
		# en supprimant l'imprimante nommée DCP1512
		DCP-1512)
			lpadmin -E -x DCP1510
		;;
		# fix pour régler le problème des imprimantes utilisant le même pilote que la DCP-1610W
		# en supprimant l'imprimante nommée DCP1610W
		DCP-1612W)
			lpadmin -E -x DCP1610W
		;;
	esac
}

# fonction de configuration du scanner
function do_config_scanner() {
	case ${CONNECTION} in
		# scanner USB
		USB)
			# on ajoute une règle udev (s'il elle n'existe pas déjà)
			if [[ ! $(grep -q "Brother" ${UDEV_RULES}) ]]; then
				sed -i '/LABEL="libsane_usb_rules_begin"/a\
\n# Brother\nATTRS{idVendor}=="04f9", ENV{libsane_matched}="yes"' ${UDEV_RULES}
			fi
		;;
		# scanner réseau
		Network)
			# brscan
			if [[ -x /usr/bin/brsaneconfig ]]; then
				brsaneconfig -a name="SCANNER" model="${MODEL}" ip=${IP}
			# brscan2
			elif [[ -x /usr/bin/brsaneconfig2 ]]; then
				brsaneconfig2 -a name="SCANNER" model="${MODEL}" ip=${IP}
			# brscan3
			elif [[ -x /usr/bin/brsaneconfig3 ]]; then
				brsaneconfig3 -a name="SCANNER" model="${MODEL}" ip=${IP}
			# brscan4
			elif [[ -x /usr/bin/brsaneconfig4 ]]; then
				sed -i '/Support Model/a\
0x029a, 117, 1, "MFC-8690DW", 133, 4\
0x0279, 14, 2, "DCP-J525W"\
0x027b, 13, 2, "DCP-J725DW"\
0x027d, 13, 2, "DCP-J925DW"\
0x027f, 14, 1, "MFC-J280W"\
0x028f, 13, 1, "MFC-J425W"\
0x0281, 13, 1, "MFC-J430W"\
0x0280, 13, 1, "MFC-J435W"\
0x0282, 13, 1, "MFC-J625DW"\
0x0283, 13, 1, "MFC-J825DW"\
0x028d, 13, 1, "MFC-J835DW"' /etc/opt/brother/scanner/brscan4/Brsane4.ini  
				brsaneconfig4 -a name=SCANNER model=${MODEL} ip=${IP}
			fi
		;;
	esac
	do_fix_config_scanner
}

# fonction de configuration du fax
function do_config_fax() {
	# on modifie les permissions du fichier /usr/lib/cups/filter/brfaxfilter
	if [[ -e /usr/lib/cups/filter/brfaxfilter ]]; then
		chmod 755 /usr/lib/cups/filter/brfaxfilter
		service cups restart
	fi
	case ${CONNECTION} in
		# fax USB
		USB)
			lpadmin -p BRFAX -v usb://Brother/${MODEL}
		;;
		# fax réseau
		Network)
			lpadmin -p BRFAX -v lpd://${IP}/binary_p1
		;;
	esac
}

# fonction pour relgler certains problemes de scanner
# je ne suis plus certain de l'utilité de cette fonction
# dans le doute je la laisse
function do_fix_config_scanner() {
	if [[ "${CODENAME}" == "oneiric" || "${CODENAME}" == "precise" || "${CODENAME}" == "quantal"  || "${CODENAME}" == "raring" ]] && [[ ${arch} == "x86_64" ]]; then
		# brscan
		if [[ -e /usr/bin/brsaneconfig ]]; then
			cp /usr/lib64/libbrcolm.so.1.0.1 /usr/lib/
			cp /usr/lib64/libbrscandec.so.1.0.0 /usr/lib/
			cp /usr/lib64/sane/libsane-brother.so.1.0.7 /usr/lib/sane/
			cp /usr/lib64/sane/libsane-brother.so /usr/lib/sane/
			cp /usr/lib64/sane/libsane-brother.so.1 /usr/lib/sane/
			cp /usr/lib64/libbrscandec.so.1 /usr/lib/
			cp /usr/lib64/libbrcolm.so /usr/lib/
			cp /usr/lib64/libbrcolm.so.1 /usr/lib/
			cp /usr/lib64/libbrscandec.so /usr/lib/
		# brscan2
		elif [[ -e /usr/bin/brsaneconfig2 ]]; then
			cp /usr/lib64/libbrscandec2.so.1.0.0  /usr/lib/
			cp /usr/lib64/sane/libsane-brother2.so.1.0.7 /usr/lib/sane/
			cp /usr/lib64/sane/libsane-brother2.so.1 /usr/lib/sane/
			cp /usr/lib64/sane/libsane-brother2.so /usr/lib/sane/
			cp /usr/lib64/libbrcolm2.so.1.0.1 /usr/lib/
			cp /usr/lib64/libbrcolm2.so /usr/lib/
			cp /usr/lib64/libbrscandec2.so.1 /usr/lib/
			cp /usr/lib64/libbrscandec2.so /usr/lib/
			cp /usr/lib64/libbrcolm2.so.1 /usr/lib/
		# BROTHER A MIS À JOUR LES PAQUETS BRSCAN3 ET BRSCAN4
		# IL N'Y A PLUS BESOIN DE COPIER LES LIBRAIRIES
		# brscan3
		#	elif [[ -e /usr/bin/brsaneconfig3 ]]; then
		#		cp /usr/lib64/libbrscandec3.so.1.0.0 /usr/lib/
		#		cp /usr/lib64/sane/libsane-brother3.so.1.0.7 /usr/lib/sane/
		#		cp /usr/lib64/sane/libsane-brother3.so.1 /usr/lib/sane/
		#		cp /usr/lib64/sane/libsane-brother3.so /usr/lib/sane/
		#		cp /usr/lib64/libbrscandec3.so /usr/lib/
		#		cp /usr/lib64/libbrscandec3.so.1 /usr/lib/
		# brscan4
		#elif [[ -x /usr/bin/brsaneconfig4 ]]; then
		#	cp /usr/lib64/sane/libsane-brother4.so.1.0.7 /usr/lib/sane/
		#	cp /usr/lib64/sane/libsane-brother4.so /usr/lib/sane/
		#	cp /usr/lib64/sane/libsane-brother4.so.1 /usr/lib/sane/
		fi
	fi
}


function do_clean_install_files() {
	if [[ -e ${TEMP_DIR} ]]; then
		rm -rf ${TEMP_DIR}
	fi
}

# lancement des différentes fonctions du programme
do_set_vars
#echo "here 1"
do_check_uid
#echo "here 2"
do_check_whiptail
#echo "here 3"
do_show_help
#echo "here 4"
do_get_family
#echo "here 5"
do_get_printer
#echo "here 6"
do_get_connection
#echo "here 7"
do_confirm
#echo "here 8"
do_prerequisites
#echo "here 9"
do_install_printer_driver
#echo "here 10"
do_install_scanner_driver
#echo "here 11"
do_install_fax_driver
#echo "here 12"
do_clean_install_files
#echo "here 13"

exit 0
